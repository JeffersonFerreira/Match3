# Match3

This is a small project for demonstration purpose.
Unity version: 2020.1.10f1

Every grid manipulation must be done via `GridController`.

Every Gem position is store inside a `Dictionary<Vector2Int, Gem>`  
I also have a `Dictionary<Vector2Int, Vector2>` where i'm storing the GridPosition to WorldPosition

`public void Initialize(Dictionary<Vector2Int, Gem> gridMap)` must be called to make it work.

You can use `GridGenerator.Generate(int rows, int columns)` or `GridUtility.Generate(int rows, int columns, Generator<Gem> func)` to return a GridMap