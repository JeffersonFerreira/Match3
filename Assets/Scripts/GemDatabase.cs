using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;

public class GemDatabase : ScriptableObject
{
	[FormerlySerializedAs("gemPrefab")] 
	[SerializeField] private Gem _gemPrefab = default;
	[FormerlySerializedAs("gemDataList")]
	[SerializeField] private GemData[] _gemDataList = new GemData[0];

	public Gem GemPrefab => _gemPrefab;
	public IReadOnlyList<GemData> GemDataList => _gemDataList;

	public static GemDatabase Instance => _instance ?? MakeInstance();
	private static GemDatabase _instance;

	// Cache instance and return it
	private static GemDatabase MakeInstance() => (_instance = Resources.Load<GemDatabase>("GemDatabase"));
}

[Serializable]
public struct GemData
{
	public Color color;
	public Sprite sprite;
	public GemType gemType;
}