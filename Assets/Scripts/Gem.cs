using System;
using UnityEngine;
using Object = UnityEngine.Object;

public class Gem : MonoBehaviour
{
	[SerializeField] private SpriteRenderer spriteRenderer;

	public bool IsDestroyed { get; private set; }

	public static event Action<Gem> OnDestroyed;

	public GemType GemType { get; private set; }

	public void Initialize(GemData gemData)
	{
		GemType = gemData.gemType;
		spriteRenderer.color = gemData.color;
		spriteRenderer.sprite = gemData.sprite;
	}

	public void Destroy()
	{
		IsDestroyed = true;
		Object.Destroy(gameObject);
		OnDestroyed?.Invoke(this);
	}

	public void MarkSelected()
	{
		
	}
}