public enum GemType
{
	Square,
	Circle,
	Diamond,
	Hexagon,
	Triangle
}