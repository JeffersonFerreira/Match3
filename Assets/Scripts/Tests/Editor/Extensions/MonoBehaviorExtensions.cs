using System.Reflection;
using UnityEngine;

namespace Tests.Editor.Extensions
{
	public static class MonoBehaviorTestUtil
	{
		public static T InvokeAwakeAndStart<T>(this T behaviour) where T : MonoBehaviour
		{
			Internal_Invoke(behaviour, "Awake");
			Internal_Invoke(behaviour, "Start");

			return behaviour;
		}

		public static T InvokeAwake<T>(this T behaviour) where T : MonoBehaviour
		{
			Internal_Invoke(behaviour, "Awake");

			return behaviour;
		}

		public static T InvokeStart<T>(this T behaviour) where T : MonoBehaviour
		{
			Internal_Invoke(behaviour, "Start");

			return behaviour;
		}

		private static void Internal_Invoke(MonoBehaviour behaviour, string methodName)
		{
			const BindingFlags flags = BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic;
			MethodInfo methodInfo = behaviour.GetType().GetMethod(methodName, flags);
			methodInfo?.Invoke(behaviour, null);
		}
	}
}