using System.Collections.Generic;
using GridSystem;
using NUnit.Framework;
using Tests.Editor.Extensions;
using UnityEngine;
using Utility;

namespace Tests.Editor
{
	public class GridControllerTests
	{
		private GridGenerator _generator;
		private GridController _controller;

		void Init(GemType[] gridGemTypes)
		{
			_controller = new GameObject().AddComponent<GridController>();
			_generator = new GameObject().AddComponent<GridGenerator>().InvokeAwakeAndStart();
			_controller.SetGridOrigin(_controller.transform);

			Gem GetGem(Vector2Int _, int index) => _generator.SpawnGem(gridGemTypes[index]);
			Dictionary<Vector2Int, Gem> grid = GridUtility.Generate(3, 3, GetGem);
			_controller.Initialize(grid);
		}

		[TestCaseSource(nameof(Source_FilterMatchSequence_FindAnySequence))]
		public void FilterMatchSequence_FindAnySequence(GemType[] gridGemTypes)
		{
			Init(gridGemTypes);

			// Act
			IReadOnlyList<GridMatchSequence> matchSequence = _controller.FilterMatchingSequence();

			// Every match sample must contain at least 1 item 
			Assert.That(matchSequence.Count, Is.EqualTo(1));
		}

		[TestCaseSource(nameof(Source_ItemsOnExactPosition))]
		public void FilterMatchSequence_ItemsOnExactPosition(GemType[] gridGemTypes, Vector2Int[] expectedPositions)
		{
			Init(gridGemTypes);

			// Act
			IReadOnlyList<GridMatchSequence> matchSequence = _controller.FilterMatchingSequence();

			// Every grid position must be the expected
			Assert.That(matchSequence[0].GridPos, Is.EquivalentTo(expectedPositions));
		}

		[Test]
		public void FilterMatchSequence_MultipleMatches()
		{
			Init(new[]
			{
				GemType.Circle, GemType.Circle, GemType.Circle,
				GemType.Triangle, GemType.Triangle, GemType.Triangle,
				GemType.Hexagon, GemType.Hexagon, GemType.Hexagon
			});

			IReadOnlyList<GridMatchSequence> matchSequence = _controller.FilterMatchingSequence();

			Assert.That(matchSequence.Count, Is.EqualTo(3));
		}

		[Test]
		public void FilterMatchSequence_NonHorizontallyOrVerticallySequenceBeingIgnored()
		{
			Init(new[]
			{
				GemType.Hexagon, GemType.Circle, GemType.Circle,
				GemType.Triangle, GemType.Circle, GemType.Hexagon,
				GemType.Hexagon, GemType.Triangle, GemType.Triangle
			});

			IReadOnlyList<GridMatchSequence> matchSequences = _controller.FilterMatchingSequence();
			Assert.That(matchSequences, Is.Empty);
		}

		[Test]
		public void FilterMatchSequence_CrossMatchBeingReturned()
		{
			Init(new[]
			{
				GemType.Hexagon, GemType.Circle, GemType.Triangle,
				GemType.Circle, GemType.Circle, GemType.Circle,
				GemType.Hexagon, GemType.Circle, GemType.Triangle
			});

			IReadOnlyList<GridMatchSequence> matchSequences = _controller.FilterMatchingSequence();
			Assert.That(matchSequences.Count, Is.EqualTo(2));
		}

		private static IEnumerable<GemType[]> Source_FilterMatchSequence_FindAnySequence()
		{
			// Horizontally on first row
			yield return new[]
			{
				GemType.Circle, GemType.Circle, GemType.Circle,
				GemType.Diamond, GemType.Diamond, GemType.Hexagon,
				GemType.Hexagon, GemType.Hexagon, GemType.Square
			};

			// Vertically on first column
			yield return new[]
			{
				GemType.Circle, GemType.Hexagon, GemType.Square,
				GemType.Circle, GemType.Diamond, GemType.Hexagon,
				GemType.Circle, GemType.Hexagon, GemType.Square
			};

			yield return new[]
			{
				GemType.Circle, GemType.Hexagon, GemType.Square,
				GemType.Circle, GemType.Diamond, GemType.Hexagon,
				GemType.Circle, GemType.Hexagon, GemType.Square
			};
		}

		private static IEnumerable<object[]> Source_ItemsOnExactPosition()
		{
			// Horizontally on first row
			yield return new object[]
			{
				new[]
				{
					GemType.Circle, GemType.Circle, GemType.Circle,
					GemType.Diamond, GemType.Diamond, GemType.Hexagon,
					GemType.Hexagon, GemType.Hexagon, GemType.Square
				},
				new[]
				{
					new Vector2Int(0, 0), new Vector2Int(1, 0), new Vector2Int(2, 0)
				}
			};

			// Vertically on first column
			yield return new object[]
			{
				new[]
				{
					GemType.Circle, GemType.Hexagon, GemType.Square,
					GemType.Circle, GemType.Diamond, GemType.Hexagon,
					GemType.Circle, GemType.Hexagon, GemType.Square
				},
				new[]
				{
					new Vector2Int(0, 0),
					new Vector2Int(0, 1),
					new Vector2Int(0, 2)
				}
			};

			// // Both Vertically and Horizontally
			// yield return new object[]
			// {
			// 	new[]
			// 	{
			// 		GemType.Circle, GemType.Circle, GemType.Circle,
			// 		GemType.Circle, GemType.Diamond, GemType.Hexagon,
			// 		GemType.Circle, GemType.Hexagon, GemType.Square
			// 	},
			// 	new[]
			// 	{
			// 		new Vector2Int(0, 0), new Vector2Int(0, 1), new Vector2Int(0, 2),
			// 		new Vector2Int(1, 0),
			// 		new Vector2Int(2, 0)
			// 	}
			// };
		}
	}
}