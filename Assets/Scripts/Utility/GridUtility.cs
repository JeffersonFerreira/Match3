using System.Collections.Generic;
using UnityEngine;

namespace Utility
{
	public delegate T Generator<out T>(Vector2Int gridPosition, int itemIndex);
	
	public static class GridUtility
	{
		public static Dictionary<Vector2Int, T> GetEmptyGrid<T>(int rows, int columns)
		{
			var data = new Dictionary<Vector2Int, T>();

			for (int row = 0; row < rows; row++)
			{
				for (int column = 0; column < columns; column++)
				{
					Vector2Int gridPos = new Vector2Int(column, row);
					data[gridPos] = default;
				}
			}

			return data;
		}
		
		public static Dictionary<Vector2Int, Gem> Generate(int rows, int columns, Generator<Gem> func)
		{
			var data = new Dictionary<Vector2Int, Gem>();

			int itemIndex = -1;
			for (int row = 0; row < rows; row++)
			{
				for (int column = 0; column < columns; column++)
				{
					itemIndex++;
					Vector2Int gridPos = new Vector2Int(column, row);

					data[gridPos] = func(gridPos, itemIndex);
				}
			}

			return data;
		}
	}
}