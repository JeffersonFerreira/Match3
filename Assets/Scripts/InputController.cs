using Extensions;
using UnityEngine;

public delegate void InputExecuted(Gem gem, Vector2Int direction);

public class InputController : MonoBehaviour
{
	public event InputExecuted OnInputExecuted;

	private Camera _camera;

	private Gem _selectedGem;
	private Vector2 _beginPos;
	private bool _isInputAllowed = true;

	private void Awake()
	{
		_camera = Camera.main;
	}

	private void Update()
	{
		// if (!_isInputAllowed)
		// 	return;

		Vector2 mousePosition = Input.mousePosition;

		if (Input.GetMouseButtonDown(0))
			InputBegin(mousePosition);

		else if (Input.GetMouseButton(0))
			InputTick(mousePosition);

		else if (Input.GetMouseButtonUp(0))
			InputEnd(mousePosition);
	}

	public void AllowInput()
	{
		_isInputAllowed = true;
	}

	private void InputBegin(Vector2 inputPos)
	{
		_beginPos = inputPos;

		Ray ray = _camera.ScreenPointToRay(inputPos);
		RaycastHit2D hit = Physics2D.Raycast(ray.origin, ray.direction);

		if (hit.transform != null && hit.transform.TryGetComponent(out Gem gem))
		{
			_selectedGem = gem;
			gem.MarkSelected();
		}
	}

	private void InputTick(Vector2 inputPos)
	{
	}

	private void InputEnd(Vector2 inputPos)
	{
		/*
		 * Converting pointer position from ScreenSpace to WorldSpace gives more
		 *  accuracy about how much distance the pointer was travelled.
		 * Since every devices was a different screen size, the result will be
		 *  weird when using the pointer position directly.
		 */
		Vector2 beingWorldPoint = _camera.ScreenToWorldPoint(_beginPos);
		Vector2 currentWorldPoint = _camera.ScreenToWorldPoint(inputPos);

		if ((currentWorldPoint - beingWorldPoint).sqrMagnitude >= 0.5f)
		{
			// Make sure only the biggest vector property is included
			Vector2Int swapDirection = (inputPos - _beginPos).ClampSingleDirection().ToVector2Int();

			OnInputExecuted?.Invoke(_selectedGem, swapDirection);

			_selectedGem = null;
			_isInputAllowed = false;
			// _gridController.SwapGemToDirection(_selectedGem, swapDirection);
		}
	}
}