using GridSystem;
using UnityEngine;

public class GameManager : MonoBehaviour
{
	private GridGenerator _gridGenerator;
	private GridController _gridController;

	private void Awake()
	{
		_gridGenerator = FindObjectOfType<GridGenerator>();
		_gridController = FindObjectOfType<GridController>();
	}
	
	private void Start()
	{
		_gridController.Initialize(_gridGenerator.Generate(5, 5));
		
		// Gem.OnDestroyed += Gem_OnDestroyed;
	}
	
	// private void OnDestroy()
	// {
	// 	Gem.OnDestroyed -= Gem_OnDestroyed;
	// }
	//
	// private void Gem_OnDestroyed(Gem obj)
	// {
	// 	_gridController.ApplyGravity();
	// }
}