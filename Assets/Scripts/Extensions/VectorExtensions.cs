using UnityEngine;

namespace Extensions
{
	public static class VectorExtensions
	{
		public static Vector2Int ToVector2Int(this Vector2 vector)
		{
			return new Vector2Int(
				Mathf.RoundToInt(vector.x),
				Mathf.RoundToInt(vector.y)
			);
		}

		public static Vector2 ClampSingleDirection(this Vector2 vector)
		{
			if (Mathf.Abs(vector.x) > Mathf.Abs(vector.y))
			{
				vector.x = Mathf.Sign(vector.x);
				vector.y = 0;
			}
			else
			{
				vector.x = 0;
				vector.y = Mathf.Sign(vector.y);
			}

			return vector;
		}
	}
}