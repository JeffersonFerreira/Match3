using System.Collections.Generic;
using System.Linq;
using GridSystem;
using UnityEngine;

namespace Extensions
{
	public static class GridControllerExtensions
	{
		public static IEnumerable<Vector2Int> GetEmptyCells(this GridController gridController)
		{
			return GetEveryCell(gridController).Where(cellKey => !gridController.TryGetGemAt(cellKey, out _));
		}

		public static IEnumerable<Vector2Int> GetEveryCell(this GridController gridController)
		{
			return gridController.PositionsGridMap.Select(pair => pair.Key);
		}
	}
}