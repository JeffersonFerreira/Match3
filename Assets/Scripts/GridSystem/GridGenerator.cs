using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Utility;
using Random = UnityEngine.Random;

namespace GridSystem
{
	public class GridGenerator : MonoBehaviour
	{
		private GemDatabase _database;
	
		private GemType[] _gemTypes;
		private Dictionary<GemType, GemData> _gemDataMap;

		private void Awake()
		{
			_database = GemDatabase.Instance;

			_gemTypes = Enum.GetValues(typeof(GemType)).Cast<GemType>().ToArray();
			_gemDataMap = _database.GemDataList.ToDictionary(d => d.gemType);
		}

		public Dictionary<Vector2Int, Gem> Generate(int rows, int columns)
		{
			return GridUtility.Generate(rows, columns, (gridPosition, itemIndex) => SpawnRandomGem());
		}
		
		public Gem SpawnRandomGem()
		{
			GemType gemType = _gemTypes[Random.Range(0, _gemTypes.Length)];
			Gem gem = Instantiate(_database.GemPrefab);
			gem.Initialize(_gemDataMap[gemType]);

			return gem;
		}

		public Gem SpawnGem(GemType gemType)
		{
			Gem gem = Instantiate(_database.GemPrefab);
			gem.Initialize(_gemDataMap[gemType]);

			return gem;
		}
	}
}