using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace GridSystem
{
	public static class GridScanner
	{
		private static readonly Vector2Int[][] SCAN_DIRECTIONS =
		{
			new[] { Vector2Int.up, Vector2Int.down },
			new[] { Vector2Int.right, Vector2Int.left }
		};

		public static IReadOnlyList<GridMatchSequence> Run(GridController gridController)
		{
			var matches = new List<GridMatchSequence>();

			// Perform a search operation for every grid position
			foreach (KeyValuePair<Vector2Int, Gem> pair in gridController.GemGridMap)
			{
				Vector2Int startPos = pair.Key;

				// Ignore invalid positions, invalid gems or empty cells
				if (!gridController.TryGetGemAt(startPos, out Gem gem))
					continue;

				// For every grid position, scan both horizontally and vertically
				foreach (Vector2Int[] scanDirection in SCAN_DIRECTIONS)
				{
					// Scan both directions, then add the origin to it
					GridPosGem[] result = scanDirection
						.SelectMany(dir => ScanDirection(gridController, startPos, dir, gem.GemType))
						.Append(new GridPosGem(startPos, gem))
						.ToArray();

					// Is unique result?
					if (result.Length >= 3 && !ContainsSameMatch(result, matches))
						matches.Add(new GridMatchSequence(result));
				}
			}

			return matches;
		}

		/// <summary>
		/// Checks every previous match results to confirm if the current grid position collection is unique 
		/// </summary>
		private static bool ContainsSameMatch(GridPosGem[] gridPos, List<GridMatchSequence> matches)
		{
			Vector2Int[] positions = gridPos.Select(g => g.GridPosition).ToArray();

			foreach (GridMatchSequence matchSequence in matches)
			{
				if (matchSequence.GridPos.All(pos => positions.Contains(pos)))
				{
					return true;
				}
			}

			return false;
		}

		// Scan a direction direction for Gems of same type
		private static IEnumerable<GridPosGem> ScanDirection(GridController gridController, Vector2Int startPos, Vector2Int direction, GemType gemType)
		{
			// starting with zero index will flood the final collection
			// with the same gem from initial position
			for (int i = 1;; i++)
			{
				Vector2Int nextPos = startPos + (direction * i);

				if (!gridController.TryGetGemAt(nextPos, out Gem gem) || gem.GemType != gemType)
					break;

				yield return new GridPosGem(nextPos, gem);
			}
		}
	}

	public readonly struct GridPosGem
	{
		public readonly Gem Gem;
		public readonly Vector2Int GridPosition;

		public GridPosGem(Vector2Int gridPosition, Gem gem)
		{
			Gem = gem;
			GridPosition = gridPosition;
		}

		public static implicit operator KeyValuePair<Vector2Int, Gem>(GridPosGem gridPosGem)
		{
			return new KeyValuePair<Vector2Int, Gem>(gridPosGem.GridPosition, gridPosGem.Gem);
		}
	}

	public readonly struct GridMatchSequence
	{
		public readonly Gem[] Gems;
		public readonly Vector2Int[] GridPos;

		public GridMatchSequence(GridPosGem[] gridPosGems)
		{
			Gems = gridPosGems.Select(pair => pair.Gem).ToArray();
			GridPos = gridPosGems.Select(pair => pair.GridPosition).ToArray();
		}
	}
}