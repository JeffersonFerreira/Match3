using System.Collections.Generic;
using System.Linq;
using Extensions;
using UnityEngine;

namespace GridSystem
{
	public class GridController : MonoBehaviour
	{
		[SerializeField] private Transform gridOrigin;

		[Header("Size")]
		[SerializeField] private float cellSize = 0.5f;
		[SerializeField] private Vector2 cellSpacing = Vector2.one * 0.12f;

		public int MaxRows { get; private set; }
		public int MaxColumns { get; private set; }
	
		public IReadOnlyDictionary<Vector2Int, Gem> GemGridMap => _gemGridMap;
		public IReadOnlyDictionary<Vector2Int, Vector2> PositionsGridMap => _positionsGridMap;

		private Dictionary<Vector2Int, Gem> _gemGridMap;
		private Dictionary<Vector2Int, Vector2> _positionsGridMap;

		private GridGenerator _generator;

		private void Awake()
		{
			_generator = FindObjectOfType<GridGenerator>();
		}

		public void Initialize(Dictionary<Vector2Int, Gem> gridMap)
		{
			MaxRows = gridMap.Max(pair => pair.Key.y);
			MaxColumns = gridMap.Max(pair => pair.Key.x);
		
			_gemGridMap = gridMap;
			_positionsGridMap = Internal_BuildGridPositionMap(gridMap);

			foreach (KeyValuePair<Vector2Int, Gem> valuePair in gridMap)
			{
				Gem gem = valuePair.Value;
				Vector2 gemWorldPos = _positionsGridMap[valuePair.Key];

				gem.transform.position = gemWorldPos;
			}
		}

		public void ApplyGravity()
		{
			// Group every empty cell by the column
			foreach (IGrouping<int, Vector2Int> grouping in this.GetEmptyCells().GroupBy(cellPos => cellPos.x))
			{
				int gridColumn = grouping.Key;

				// Find the lowest empty cell and apply gravity to every gem above it
				int lowestY = grouping.Max(cellPos => cellPos.y);

				int cellsToFall = 1;
				for (int y = lowestY - 1; y >= 0; y--)
				{
					Vector2Int cellPos = new Vector2Int(gridColumn, y);

					// Accumulate the amount of cells that the next Gem should fall  
					if (!TryGetGemAt(cellPos, out Gem gem))
					{
						cellsToFall++;
						continue;
					}

					Vector2Int newGemPos = new Vector2Int(gridColumn, y + cellsToFall);

					// Swap gem position from grid
					_gemGridMap[cellPos] = null;
					_gemGridMap[newGemPos] = gem;

					// Update Gem from grid position map
					gem.transform.position = _positionsGridMap[newGemPos];
				}
			}
		}

		public void SwapGemToDirection(Gem gem, Vector2Int direction)
		{
			/*
		 * Must invert the vertical axis because the positive values goes down and negative goes up.
		 * This problem comes from how the grid is generated.
		 * [(0 0), (1 0),
		 *  (0 1), (1 1),
		 *  (0 2), (1 2)]
		 *
		 * World be strange to pass (0 2) to get the first row.
		 */
			direction.y *= -1;

			// Get the selected gem grid position
			if (!TryFindGemGridPosition(gem, out Vector2Int oldGridPos))
			{
				Debug.LogError("Gem Cell not found");
				return;
			}

			Vector2Int newGridPos = oldGridPos + direction;

			// Get the get at new position
			if (!TryGetGemAt(newGridPos, out Gem otherGem))
				return;

			// Swap gem positions
			_gemGridMap[newGridPos] = gem;
			_gemGridMap[oldGridPos] = otherGem;

			// Update gem world position
			gem.transform.position = _positionsGridMap[newGridPos];
			otherGem.transform.position = _positionsGridMap[oldGridPos];
		}

		public bool TryGetGemAt(Vector2Int pos, out Gem gem)
		{
			gem = null;
			if (!_gemGridMap.TryGetValue(pos, out Gem g) || g == null)
				return false;

			if (g.IsDestroyed)
				return false;

			gem = g;
			return true;
		}

		public bool TryFindGemGridPosition(Gem gem, out Vector2Int gridPosition)
		{
			foreach (KeyValuePair<Vector2Int, Gem> valuePair in _gemGridMap)
			{
				if (valuePair.Value == gem)
				{
					gridPosition = valuePair.Key;
					return true;
				}
			}

			gridPosition = default;
			return false;
		}

		public IReadOnlyList<GridMatchSequence> FilterMatchingSequence()
		{
			return GridScanner.Run(this);
		}

		private Dictionary<Vector2Int, Vector2> Internal_BuildGridPositionMap(Dictionary<Vector2Int, Gem> gridMap)
		{
			var data = new Dictionary<Vector2Int, Vector2>();

			Vector2 originPos = gridOrigin.position;
			foreach (KeyValuePair<Vector2Int, Gem> valuePair in gridMap)
			{
				Vector2Int gridPos = valuePair.Key;

				Vector2 worldPos = new Vector2(gridPos.x * cellSize, -gridPos.y * cellSize);
				worldPos.Scale(Vector2.one + cellSpacing);
				worldPos += originPos;

				data[gridPos] = worldPos;
			}

			return data;
		}

		public void SetGridOrigin(Transform gridOrigin)
		{
			this.gridOrigin = gridOrigin;
		}

		public void SpawnGemAt(Vector2Int gridPosition)
		{
			Gem gem = _generator.SpawnRandomGem();

			_gemGridMap[gridPosition] = gem;
			gem.transform.position = _positionsGridMap[gridPosition];
		}
	}
}