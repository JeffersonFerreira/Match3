using System.Collections.Generic;
using System.Linq;
using GridSystem;
using UnityEngine;

public class GameFlowController : MonoBehaviour
{
	private GridController _gridController;
	private InputController _inputController;
	
	private void Awake()
	{
		_gridController = FindObjectOfType<GridController>();
		_inputController = FindObjectOfType<InputController>();
	}

	private void Start()
	{
		_inputController.OnInputExecuted += Player_OnSwapGems;
	}

	private void Player_OnSwapGems(Gem gem, Vector2Int direction)
	{
		// TODO: Add animation

		_gridController.SwapGemToDirection(gem, direction);

		IReadOnlyList<GridMatchSequence> matchSequences = _gridController.FilterMatchingSequence();
		
		if (matchSequences.Count == 0)
		{
			// _gridController.SwapGemToDirection(gem, -direction);
		}
		else
		{
			foreach (Gem matchGem in matchSequences.SelectMany(sequence => sequence.Gems))
			{
				matchGem.Destroy();
			}
		}
		
		_gridController.ApplyGravity();
		SpawnMoreGems();
		_inputController.AllowInput();
	}

	private void SpawnMoreGems()
	{
		int rows = _gridController.MaxRows;

		// Group by columns
		foreach (IGrouping<int,Vector2Int> grouping in _gridController.GemGridMap.Keys.GroupBy(gridPos => gridPos.x))
		{
			int column = grouping.Key;
			int numOfGems = grouping.Count();

			int gemsToSpawn = rows - numOfGems;
			
			for (int spawnGemRow = gemsToSpawn - 1; spawnGemRow >= 0; spawnGemRow--)
			{
				_gridController.SpawnGemAt(new Vector2Int(column, spawnGemRow));
			}
		}
	} 
}